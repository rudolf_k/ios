# MovieList

For users who want to be up to date with the latest movies, check the ratings and short descriptions.

## General

* iOS Version: 13+
* Swift Version: 5
* Dependency Management: [CocoaPods](https://cocoapods.org)

## Prerequirements

* [bundler](https://bundler.io)

## How to use this repository

1. Clone this repository locally via command line or SourceTree.

```bash
https://gitlab.com/halcyonmobile/ubb-collaboration/ios/demos/assignments/ubbioscourse.git
```

## What's included

### Architecture

* Directory structure
* Flow controllers

### Frameworks

* [SwiftGen](https://github.com/SwiftGen/SwiftGen) - Generate precompiled files for assets and resources.
* [SwiftLint](https://github.com/realm/SwiftLint) - Swift linter to enforce code style rules

//
//  User.swift
//  project-skeleton
//
//  Created by Halcyon Mobile on 12/03/2017.
//  Copyright © 2017 Halcyon Mobile. All rights reserved.
//

import Foundation

struct User: Decodable {
    let name: String
}

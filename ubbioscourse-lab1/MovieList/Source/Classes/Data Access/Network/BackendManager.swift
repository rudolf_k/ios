//
//  BackendManager.swift
//  packmen
//
//  Created by Alexandra Muresan on 20/07/2018.
//  Copyright © 2018 Halcyon Mobile. All rights reserved.
//

import Foundation

public struct ErrorMessages {
    public static let unknownError = "Oops! Something went wrong while processing your request."
    public static let invalidUrl = "URL is not valid:"
    public static let invalidParameters = "Invalid parameters!"
    public static let missingURL = "URL request to encode was missing a URL"
    public static let invalidData = "the data returned is not a valid JSON"
}

final class BackendManager {

    private static let baseURL = "https://pack-men.herokuapp.com/api/v1"
    private static var headers: HTTPHeaders = ["Content-Type": "application/json", "Accept": "application/json"]

    private static let defaultSession = URLSession(configuration: .default)

    static func execute<T: Decodable>(request: RequestRepresentable, completion: @escaping (Result<T, Error>) -> Void) {
        // implement the network request for here
        guard let error = InterfaceError(httpStatusCode: 403) else { return }
        completion(.failure(error))
    }

    static func execute<T: Decodable>(request: RequestRepresentable, completion: @escaping (Result<[T], Error>) -> Void) {
        // implement the network request for arrays here
        guard let error = InterfaceError(httpStatusCode: 400) else { return }
        completion(.failure(error))
    }
}

/**
 Error object to use on the presentation layer.
 
 The message may be displayed to the user, so it should be friendly.
 */
open class InterfaceError: Error {

    public let message: String

    public init(message: String) {
        self.message = message
    }

    public init(error: Error?) {
        if let message = error?.localizedDescription { 
            self.message = message
        } else {
            self.message = ErrorMessages.unknownError
        }
    }

    public init?(httpStatusCode: Int) {
        switch httpStatusCode {
        case 200 ..< 300:
            return nil
        case 400:
            self.message = "Bad Request"
        case 401:
            self.message = "Unauthorized"
        case 403:
            self.message = "Forbidden"
        case 500:
            self.message = "Internal Server Error"
        case 503:
            self.message = "Service unavailable"
        default:
            return nil
        }
    }
}

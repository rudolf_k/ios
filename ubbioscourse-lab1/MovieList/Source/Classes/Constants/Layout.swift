//
//  Layout.swift
//  Workeepr
//
//  Created by Hanna Kovacs on 27/10/2020.
//

import struct CoreGraphics.CGFloat

public extension CGFloat {
    /// 2
    static let smallCornerRadius: CGFloat = 2
    /// 4
    static let cornerRadius: CGFloat = 4
    /// 6
    static let mediumCornerRadius: CGFloat = 6
    /// 8
    static let bigCornerRadius: CGFloat = 8

    /// 2
    static let smallPadding: CGFloat = 2
    /// 4
    static let halfPadding: CGFloat = 4
    /// 8
    static let padding: CGFloat = 8
    /// 16
    static let padding2x: CGFloat = padding * 2
    /// 24
    static let padding3x: CGFloat = padding * 3
    /// 32
    static let padding4x: CGFloat = padding * 4
    /// 48
    static let padding6x: CGFloat = padding * 6

    /// 0.5
    static let mediumOpacity: CGFloat = 0.5
    /// 1
    static let fullOpacity: CGFloat = 1.0
}

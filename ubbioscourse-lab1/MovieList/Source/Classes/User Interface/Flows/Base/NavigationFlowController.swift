//
//  NavigationFlowController.swift
//  project-skeleton
//
//  Created by Halcyon Mobile on 5/17/17.
//  Copyright © 2017 HalcyonMobile. All rights reserved.
//

import UIKit

enum FlowPresentationStyle {
    case push, present
}

protocol FlowControllerDelegate: class {
    func didFinish(on flowController: FlowController)
}

class NavigationFlowController: BaseFlowController {

    private(set) var presentationStyle: FlowPresentationStyle = .push

    private(set) var navigationController: UINavigationController

    override var mainViewController: UIViewController? {
        return navigationController
    }

    // MARK: - Lifecycle

    required init(from parent: FlowController?) {
        if let flow  = parent as? NavigationFlowController {
            navigationController = flow.navigationController
        } else {
            navigationController = UINavigationController()
        }

        super.init(from: parent)
    }

    // MARK: - Flow cycle

    override func start() {
        start(presentationStyle: .push)
    }

    func start(presentationStyle: FlowPresentationStyle) {
        self.presentationStyle = presentationStyle

        switch presentationStyle {
        case .push:
            startPushed()
        case .present:
            startPresented()
        }

        super.start()
    }

    // MARK: - Helpers

    fileprivate func startPushed() {
        navigationController.pushViewController(firstScreen, animated: true)
    }

    fileprivate func startPresented() {
        let navController = UINavigationController(rootViewController: firstScreen)
        navigationController.present(navController, animated: true)

        navigationController = navController
    }
}

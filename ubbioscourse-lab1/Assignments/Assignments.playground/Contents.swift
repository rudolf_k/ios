//: Playground - noun: a place where people can play

//iOS Programozás, választható tantárgy 2022
//1. Labor Playground és bevezetés a Swift nyelvbe

import Foundation

// VARIABLES

// 1. (0.5 p) Declare a constant array favouriteNumbers with values for your favourite numbers.
let favouriteNumbers = [42, 7, 15, 24]


// 2. (0.5 p) Declare a dictionary with the name of your colleagues and the "department" he/she is working on.
var colleagueDict = ["Peti" : "Sanitation",
                     "Jani" : "Management",
                     "Bela" : "Tech Support",
                     "Pista": "Tech Support"]


// 3. (0.5 p) Declare an array of tuples with the list of your colleagues and their height.
// ex. tuple: ("Big Guy", 201)
var colleagueHeights = [("Peti", 157), ("Jani", 186), ("Bela", 175), ("Pista", 175)]


// 4. (0.5 p) Order the array created above in ascending order (based on height)
// use `sorted`
colleagueHeights = colleagueHeights.sorted(by: { $0.1 < $1.1 })


// 5. (0.5 p) Group the dictionary by department.
// use the: `Dictionary(grouping: ..., by: ...)` initializer
var colleaguesByDepartment = Dictionary(grouping: colleagueDict, by: {$0.1})


// 6. (0.5 p) Order the tuple based on the heights, if there are entries with the same height, order them by alphabetical order.
// hint: the `sorted` function can take two propeties as parameter
colleagueHeights = colleagueHeights.sorted(by: { 
    if ($0.1 != $1.1) {
        return ($0.1 < $1.1)
    } else {
        return $0.0 < $1.0
    }
 })


// 7. (0.5 p) Increment every number by one in favouriteNumbers.
// use `map`
var favouriteNumbersIncremented = favouriteNumbers.map { $0 + 1 }


// 8. (0.5 p) Print out only the name of your colleagues from the dictionary.
print (colleagueDict.map { $0.0 })


// 9. (1 p) Print out the average height of your colleagues.
// use `map` + `reduce`
print ((colleagueHeights.map { $0.1 }).reduce(0, { x, y in x + y }) / colleagueHeights.count)


// 10. (1 p) Split the array in half.
// hint: you can access a subarray like this: `someArray[..<n]` or `someArray[n...]`
var halfArray = favouriteNumbers[..<(favouriteNumbers.count / 2)]


// 11. (1 p) Declare the first 10 Fibonacci numbers in a set. (1,1,2,3,...)
// Obviously the number 1 will appear only once in this set (properties of a set)
// write the algorithm, (implement a function like this: `func fibonacci(n: Int) -> Set<Int>`)
func fibonacci(n : Int) -> Set<Int> {
    var fiboNumbers: Set<Int> = []
    if n > 0 {
        fiboNumbers.insert(1)
    }
    if n > 2 {
        fiboNumbers.insert(2)
    }
    
    var nn = n-3
    var a = 1
    var b = 2
    var c = -1
    while nn > 0 {
        c = a+b
        fiboNumbers.insert(c)
        a = b
        b = c
        nn = nn-1
    }

    return fiboNumbers
}
print(fibonacci(n: 10))


// 12. (1 p) Get the intersect of two sets: the first 10 Fibonacci numbers and the first 10 prime numbers.
// This time you won't need to write the alorithm of the Fibonacci numbers. Just put them in a list, and use that list.
// there is a method `intersection`
var fibonacciSet = fibonacci(n: 10)
var primeSet: Set = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]
print (fibonacciSet.intersection(primeSet))


// CONTROL FLOW

// 13. (1 p) Take a positive number and print the reversed version of it.
// write the algorithm (implement a function like this: `func reverse(number: Int) -> Int`)
func reverse(number: Int) -> Int {
    var n = number
    var reverse = 0

    while n > 0 {
        reverse = reverse * 10 + (n % 10)
        n = n / 10
    }

    return reverse
}


// 14. (1 p) Choose a number and find out if it's palindrome.
// use the solution from above
func palindromeCheck(number: Int) -> Bool {
    return number == reverse(number: number)
}
print (palindromeCheck(number: 12321))


// CLASSES AND STRUCTS

// 15. (1 p) Write a struct named Course which will have a String property name and an Int one mark.
// ex. struct Sth { aProperty: Type, ..}
struct Course {
    var name: String
    var mark: Int
}


// 16. (1 p) Create a class named Student which will have a private String property name and a private array of courses which he attends to. Both properties will be initialized in the constructor.
class Student {
    private var name: String
    private var courses: [Course]
    
    init() {
        name = ""
        courses = []
    }
    init(name: String, courses: [Course]) {
        self.name = name
        self.courses = courses
    }
}


// ENUMS

// 17. (1 p) Create an enum type which will have two cases: normal and scholar.
// ex. enum Sth { case fist, case last }
enum MyEnum {
    case normal
    case scholar
}


// 18.( 1 p) Create a String property in the enum (e.g. description) which will return a textual representation of each case (e.g. "This is a scholar student.").
// use the `switch` statement for setting the right textual representation
enum MyEnum2 {
    case normal
    case scholar

    var description: String {
        switch (self) {
            case .normal:
                return "This is a normal student."
            case .scholar:
                return "This is a scholar student."
        }
    }
}


// 19. (1 p) Add this enum as a property to the above mentioned Student class.
// 20. (1 p) Modify the Student class constructor so the type type parameter has a default value of .normal).
// HINT: you can give a default value in the constructor like `init(..., type: Type = .aType)`
class Student2 {
    private var name: String
    private var courses: [Course]
    var type: MyEnum2
    
    init(name: String = "", courses: [Course] = [], type: MyEnum2 = .normal) {
        self.name = name
        self.courses = courses
        self.type = type
    }
}


// PROTOCOLS

// 21. (1 p) Vehicles can have different properties and functionality.
// All Vehicles:
// • Have a speed at which they move
// • Calculate the duration it will take them to travel a certain distance
// All Vehicles except a Motorcycle
// • Have an amount of Windows
// Only Buses:
// • Have a seating capacity
// Create the following Vehicles types: Car, Bus, Motorcycle. Do not use subclassing, use protocols instead. Create an array over the minimum required protocol and put an instance of every type in it and print the the following text for every item of the array (if it's possible): " *type* has *amount of windows* windows and needs *time* to travel 100 kilometers. "

protocol Vehicle {
    var speed: Double { get set }
    mutating func calculateDuration(distance: Double) -> Double
    func description() -> String
}

protocol WindowVehicle: Vehicle {
    // TODO: add the needed property(s)
    var numberOfWindows: Int { get set }
}

struct Bus: WindowVehicle {
    var speed: Double
    func calculateDuration(distance: Double) -> Double {
        return distance * 60 / speed
    }
    
    // TODO: implement what's needed and add the needed property(s)
    var numberOfWindows: Int
    var seatingCapacity: Int

    func description() -> String {
        "Bus has \(numberOfWindows) windows, \(seatingCapacity) seating capacity and needs \(calculateDuration(distance: 100)) to travel 100 kilometers."
    }
}

// TODO: implement the other types
struct Car: WindowVehicle {
    var speed: Double
    func calculateDuration(distance: Double) -> Double {
        return distance * 60 / speed
    }
    var numberOfWindows: Int

    func description() -> String {
        return "Car has \(numberOfWindows) windows and needs \(calculateDuration(distance: 100)) to travel 100 kilometers."
    }
}

struct MotorCycle: Vehicle {
    var speed: Double
    func calculateDuration(distance: Double) -> Double {
        return distance * 60 / speed
    }

    func description() -> String {
        return "Motorcycle needs \(calculateDuration(distance: 100)) to travel 100 kilometers."
    }
}


// TODO: add elements to this array
var vehicles: [Vehicle] = []

vehicles.append(Bus(speed: 60, numberOfWindows: 18, seatingCapacity: 30))
vehicles.append(Car(speed: 80, numberOfWindows: 6))
vehicles.append(MotorCycle(speed: 70))

for vehicle in vehicles {
    print(vehicle.description())
}


// EXTENSTIONS

// 22. (1 p) Create Int extensions for:
// • Radian value of a degree (computed property)
// • Array of digits of Int (computed property)
extension Int {
    
    func toRadian () -> Double {
        return Double.pi * Double(self) / 180
    }
    
    // TODO: Array of digits
    func arrayOfDigits () -> [Int] {
        var digits: [Int] = []
        var n = self
        while n > 0 {
            digits.append(n % 10)
            n = n / 10
        }
        return digits
    }
}


// 23. (1 p) Create String extensions for:
// • Check if the string contains the character 'a' or 'A'
extension String {
    func checkA () -> Bool {
        return self.contains("a") || self.contains("A")
    }
}




// 24. (1 p) Create Date extensions for:
// • Check if a date is in the future (computed property)
// • Check if a date is in the past (computed property)
// • Check if a date is in today (computed property)

extension Date{
    func isToday() -> Bool {
        return Calendar.current.isDateInToday(self)
    }
    
    // TODO: is in the future
    func isInTheFuture() -> Bool {
        return self > Date()
    }
    
    // TODO: is in the past
    func isInThePast() -> Bool {
        return self < Date()
    }
}

let formatter = DateFormatter()
formatter.dateFormat = "yyyy/MM/dd"
let date: Date = formatter.date(from: "2018/03/20")!
let today = Date()
// TODO: test the written extension
print(today.isToday())      // expecting: true
print(date.isInTheFuture()) // expecting: false
print(date.isInThePast())   // expecting: true


// CLOSURES

// 25. (1 p) Declare a closure that takes an Integer as an argument and returns if the number is odd or even (using regular syntax and shorthand parameter names)
// ex: `let empty: (String) -> Bool = { $0.isEmpty }` - closure for testing if a string is empty
let odd: (Int) -> Bool = { $0 % 2 == 0 }


// 26. (1 p) Use the above defined closure to filter out odd numbers from an array of random numbers (use the filter function)
func makeRandomList() -> [Int] {
    return [1, 43, 654, 32, 67, 23, 78, 94, 12, 67]
}
// TODO: do the filtering
var filteredList = makeRandomList().filter {odd($0) }



// 27. (1 p) Declare a closure that takes 2 Integers as parameters and returns true if the first argument is larger than the second and false otherwise (using regular syntax and shorthand parameter names)
let firstLarger: (Int, Int) -> Bool = { $0 > $1 ? true : false }






